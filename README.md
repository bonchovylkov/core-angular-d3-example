﻿## How to run the project

1. Open the project with Visual Studio 2017
2. Restore npm packages
3. Run gulp dev


##SPA web project created on .NET Core with Angular, using systemjs for module loading, gulp for automating the the tasks of build and move. Added the famous d3 library with example of Pie chart.