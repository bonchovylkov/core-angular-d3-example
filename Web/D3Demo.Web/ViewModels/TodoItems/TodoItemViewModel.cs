﻿namespace D3Demo.Web.ViewModels.TodoItems
{
    using D3Demo.Common.Mapping;
    using D3Demo.Data.Models;

    public class TodoItemViewModel : IMapFrom<TodoItem>
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public bool IsDone { get; set; }
    }
}
