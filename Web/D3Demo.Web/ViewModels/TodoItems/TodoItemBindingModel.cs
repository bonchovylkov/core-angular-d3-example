﻿namespace D3Demo.Web.ViewModels.TodoItems
{
    using System.ComponentModel.DataAnnotations;

    using D3Demo.Common.Mapping;
    using D3Demo.Data.Models;

    public class TodoItemBindingModel : IMapTo<TodoItem>
    {
        [Required]
        public string Title { get; set; }
    }
}
