﻿import { Component } from '@angular/core';
import { NvD3Component } from "ng2-nvd3";

//import 'd3';
//import 'nvd3';


@Component({
    moduleId: module.id,
    selector: 'home',
    templateUrl: 'home.component.html'
})

export class HomeComponent {

    options: any;
    data: any;

    constructor() {
        this.options = {
            chart: {
                type: 'pieChart',
                height: 500,
                x: function (d) {
                    return d.key;
                },
                y: function (d) {
                    return d.y;
                },
                showLabels: true,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 5,
                        bottom: 5,
                        left: 0
                    }
                }
            }
        };

        this.data = [
            {
                key: "P60-1",
                y: 256
            },
            {
                key: "P60-2",
                y: 445
            },
            {
                key: "P40",
                y: 225
            },
            {
                key: "P73",
                y: 127
            },
            {
                key: "P71",
                y: 128
            }
        ];
    }
}
