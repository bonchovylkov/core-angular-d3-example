﻿namespace D3Demo.Common
{
    public static class GlobalConstants
    {
        public const string AdministratorRoleName = "Administrator";

        public const string JsonContentType = "application/json";
    }
}
